# Html and css exercise #

This exercise is comoposed of 3 tasks, each task asks you to recreate a website, each task should be done in a separated branch, meaning you will create 3 branchs (feature/<your name>_{task1|task2|task3}) and 3 pull requests.

Your work should be organized in the following file structure

- Folder with your name
    - task1
        - all files of task1
    - task2
        - all files of task2
    - task3
        - all files of task3


## Task source

The websites for each task can be found [here](https://tutor-html-css.web.app/), you don't need to copy exact colors but the behavior of the page should be the same.

IMPORTANT: You don't need to make a navigation bar to navigate between tasks, i made it just for simplicity.

## Task 1

the website is a simple square that changes color when hovered.  

The objective here is just to make you more comfortable with positioning.

## Task 2

the website is a collection of cards, each displaying a different character from a different anime, each card has a link for the my anime list page of the correspondent anime.

Be aware of the behavior of the cards in smaller screens and the behavior of the cards when hovered. Also, note that you are expected to change the default colors of the links(both before and after they are pressed).

## Task 3

The website has a basic layout, a banner, content and a footer. Is a simple page that displays some musics names and their correspondent youtube link.

Be aware of the behavior of the music cards when hovered and the behavior of the whole page when in smaller screens sizes.


## images

You are free to use any image you wish, but if you want to use the same ones that i used, they will be available in the drive.

